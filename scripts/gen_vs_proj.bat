@echo off
set ide_dir=..\ide\visual_studio
if not exist %ide_dir% mkdir %ide_dir%
pushd %ide_dir%
call cmake ../.. -G "Visual Studio 14 Win64"
popd
