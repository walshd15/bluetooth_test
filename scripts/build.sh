#!/bin/bash

if ! hash cmake 2>/dev/null; then
    echo "CMake not found. CMake is required to build!"
    exit 1
fi

cmake .. -B../build || exit $?
cmake --build ../build
