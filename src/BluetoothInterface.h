#pragma once 

#include <string>

namespace BluetoothInterface
{

extern bool RunCommand( std::string command );

}