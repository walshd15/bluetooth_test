#include <fstream>
#include "FileIO.h"
#include <iostream>

//TODO - Exceptions

namespace FileIO
{

std::vector<char> ReadFileAsBytes(const char* filepath )
{
	//TODO - This can fail
    std::ifstream inFile( filepath, std::ios::ate );
    std::ifstream::pos_type pos = inFile.tellg();

    //TODO - Will give bad alloc if file isn't found because pos will be -1. Should check this
    std::vector<char> result(pos);

    inFile.seekg( 0, std::ios::beg );
    inFile.read( result.data(), pos );

    inFile.close();

    return result;
}

void SaveBytesToFile( const std::vector<char> bytes, const char* const filepath )
{
	//TODO - This can fail
 	std::ofstream outFile ( filepath );
    outFile.write( bytes.data(), bytes.size() );
    outFile.close();
}

}