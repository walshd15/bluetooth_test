#include <vector>

namespace FileIO
{
	extern std::vector<char> ReadFileAsBytes( const char* filepath );
	extern void SaveBytesToFile( const std::vector<char> bytes, const char* const filepath);
}
