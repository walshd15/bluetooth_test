#include "BluetoothInterface.h"

#include <functional>
#include <unordered_map>
#include <iostream>
#include <string>

//TODO - Need a way to pass parameters

namespace BluetoothInterface
{

void TestMethod()
{
	std::cout << "You callled a method." << std::endl;
	std::cout << "Well done :)" << std::endl;
	
	std::cout << "I hope you're proud." << std::endl;
}

const std::unordered_map< std::string, std::function<void()>> commands
{
	{ "Test", TestMethod }
};


bool RunCommand( std::string command )
{
	try
	{
		std::function<void()> func;
		func = commands.at( command );
		func();
		return true;
	}
	catch( const std::out_of_range& outOfRangeException ) 
	{
		std::cout << "Command not found" << std::endl; 
		return false;
	}
}

}