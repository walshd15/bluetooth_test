#include <vector>


class BluetoothManager
{
public:
	BluetoothManager();
	~BluetoothManager();

public:
	void PrintDevices();
	void RunServer();
	void RunClient();
	bool SetDiscoverable( bool discoverable );

	bool SendFile( const int socketID, const char* const filepath );
	std::vector<char> ReceiveFile( const int clientID );
};