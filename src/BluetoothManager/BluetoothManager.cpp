#include "BluetoothManager.h"

#include "../FileIO.h"
#include "../BluetoothInterface.h"

#include <iostream>

#include <stdlib.h>
#include <unistd.h>

#include <sys/socket.h>

//Note - Order of these includes matters :(
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>
#include <bluetooth/rfcomm.h>

#include <sys/ioctl.h>

static const int _sMaxResponses{ 255 };

const static bdaddr_t _sBluetoothAddressAny{ { 0, 0, 0, 0, 0, 0 } };
const static bdaddr_t _sBluetoothAddressAll{ { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff } };

//TODO - split this into a linux impl

BluetoothManager::BluetoothManager()
{
}

BluetoothManager::~BluetoothManager()
{
}

void BluetoothManager::RunServer()
{
    int status{ 0 };

    if( !SetDiscoverable( true ) )
    {
        perror( "Failed to make device discoverable" );
        return;
    }

    sockaddr_rc loc_addr{ 0 };
    sockaddr_rc rem_addr{ 0 };
    
    char buffer[1024] = { 0 };
    socklen_t opt = sizeof(rem_addr);

    // allocate socket
    const int socketID = socket(AF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM);
    printf("Allocated Socket %d\n", socketID );

    // bind socket to port 1 of the first available 
    // local bluetooth adapter
    loc_addr.rc_family = AF_BLUETOOTH;
    loc_addr.rc_bdaddr = _sBluetoothAddressAny;
    loc_addr.rc_channel = (uint8_t) 1;

    status = bind( socketID, (sockaddr*) &loc_addr, sizeof( loc_addr ) );
    if( status < 0  )
    {
        perror( "Failed to bind socket \n" );
        return;
    }

    // put socket into listening mode
    status = listen( socketID, 1 );
    if( status < 0  )
    {
        perror( "Failed to put socket into listening mode\n" );
        return;
    }

    printf( "Waiting for client connection\n" );
    // accept one connection
    const int clientID = accept( socketID, (sockaddr *)&rem_addr, &opt );

    ba2str( &rem_addr.rc_bdaddr, buffer );
    fprintf( stderr, "accepted connection from %s\n", buffer );
    memset( buffer, 0, sizeof(buffer) );

    printf( "Reading data from client\n" );
    // read data from the client
    
    if( !SendFile( clientID, "../src/BluetoothInterface.h" ) )
    {
        perror( "Failed to write data\n" );
    }
 
    const int bytes_read = read( clientID, buffer, sizeof( buffer ) );
    if( bytes_read > 0 ) {
        printf("received [%s]\n", buffer);
        BluetoothInterface::RunCommand( buffer );
    }

    SetDiscoverable( false );

    // close connection
    close( clientID );
    close( socketID );
}

void BluetoothManager::RunClient()
{
    int socketID{ 0 };
    int status{ -1 };
    //TODO - Don't hardcode this
    // Virtualbox: 00:1A:7D:DA:71:13
    // Lappy: C4:8E:8F:F5:DD:C8
    // Pi: 5C:F3:70:83:49:B2
    const char dest[18] = "5C:F3:70:83:49:B2";

    printf( "Starting client\n" );

    // allocate a socketID
    socketID = socket( AF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM );

    // set the connection parameters (who to connect to)
    sockaddr_rc addr = { 0 };
    addr.rc_family = AF_BLUETOOTH;
    addr.rc_channel = (uint8_t) 1;
    //String to bluetooth adress
    str2ba( dest, &addr.rc_bdaddr );

    // connect to server
    status = connect( socketID, (struct sockaddr *)&addr, sizeof(addr));

    // send a message
    if( status == 0 ) 
    {
        if( !SendFile( socketID, "../src/BluetoothInterface.h" ) )
        {
           perror( "Failed to write data\n" );
        }
    
        status = write( socketID, "Test", 4 );
    }
    else if( status < 0 )
    {
        perror( "Connection Error!\n" );
    }

    close( socketID );
}

void BluetoothManager::PrintDevices()
{
    //TODO - Not sure these make sense as member variables anymore
    int deviceID = hci_get_route( nullptr );
    int socketID = hci_open_dev( deviceID );

    if( deviceID < 0 || socketID < 0 )
    {
        perror("opening socket");
        //TODO - This should probably throw
    }

    char addr[19] = { 0 };
    char name[248] = { 0 };

    printf( "Starting scan\n" );

    const int len{ 8 };
    //Flush the cache of previously discovered devices
    const int flags = IREQ_CACHE_FLUSH;
    //TODO - Rename to discoveredDevices or something
    inquiry_info *ii = new inquiry_info[_sMaxResponses ];
    
    //Start discovery and store list of devices in ii
    //Discovery lasts at most 1.28 * len seconds
    const int num_rsp = hci_inquiry( 
        deviceID, 
        len, 
        _sMaxResponses, 
        nullptr, 
        &ii, 
        flags );

    if( num_rsp < 0 ) 
    {
        perror( "hci_inquiry" );
    }

    if( num_rsp == 0 )
    {
        printf( "No devices found!\n" );
    }
    else
    {
        printf( "Found devices:\n" );
        for( int i = 0; i < num_rsp; i++ ) 
        {
            //Bluetooth address to string
            ba2str( &(ii+i)->bdaddr, addr );
            //Query the name of the discovered device. 
            //TODO - Last parameter is timeout, should it be 0?
            const int result = hci_read_remote_name(
                socketID, 
                &(ii+i)->bdaddr, 
                sizeof(name), 
                name, 
                0 );        

            if( result < 0 )
            {
                strcpy( name, "[unknown]" );
            }

            printf( "%s  %s\n", addr, name );
        }
    }

    free( ii );
    close( socketID );
}

bool BluetoothManager::SetDiscoverable( bool discoverable )
{
    int deviceID = hci_get_route( nullptr );
    int socketID = socket( AF_BLUETOOTH, SOCK_RAW, BTPROTO_HCI );

    if( deviceID < 0 )
    {
        printf("Failed to get device handle\n" );
    }
    if( socketID < 0 )
    {
        printf("Failed to get HCI socket handle\n" );
    }

    struct hci_dev_req dr;
    dr.dev_id = deviceID;
    dr.dev_opt = discoverable ? ( SCAN_PAGE | SCAN_INQUIRY ) : SCAN_DISABLED;

    if( ioctl(
        socketID, 
        HCISETSCAN, 
        (unsigned long) &dr ) < 0 ) 
    {
        printf("Failed to set device as discoverble\n Are you root?\n");
        return false;
    }

    close( socketID );
    return true;
}

bool BluetoothManager::SendFile( const int socketID, const char* const filepath )
{
    int status{ 0 };
    //TODO - This requires an active connection. Should it take a socket handle or something?
    std::vector<char> bytes = FileIO::ReadFileAsBytes( filepath );
    status = write( socketID, bytes.data(), bytes.size() );

    return status >= 0;
}

std::vector<char> BluetoothManager::ReceiveFile( const int clientID )
{
    std::vector<char> bytes;
    //TODO - Would make more sense if you received a size before the file to know how much to allocate
    bytes.resize( 1024 );
    const int bytesRead = read( clientID, bytes.data(), bytes.size() );
    
    if( bytesRead <= 0 )    
    {
        std::cout << "Failed to read file" << std::endl;
    }

    return bytes;
}
