#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <cassert>

#include "BluetoothManager/BluetoothManager.h"

static std::vector<std::string> sCommands{ "scan", "run_client", "run_server" };

void PrintInputError()
{
	std::cout << "You must specify one of the following commands:\n";

	for( const std::string& command : sCommands )
	{
		std::cout << command << "\n";
	}
}

int main( int argc, char **argv ) 
{

	//TODO - Really should ask who to connect to. At least when running the client
	if( argc < 2 )
	{
		PrintInputError();
	}
	else
	{
		const auto& iterator = std::find( sCommands.begin(), sCommands.end(), argv[1] );

		if( iterator == sCommands.end() )
		{
			PrintInputError();
		}
		else
		{
			BluetoothManager bMgr;

			//TODO - There must be a nicer way to do all this...
			const int index = std::distance( sCommands.begin(), iterator );
			switch( index )
			{
				case 0:
				{
					assert( sCommands[index] == "scan" );
					bMgr.PrintDevices();
				} break;

				case 1:
				{
					assert( sCommands[index] == "run_client" );
					bMgr.RunClient();
				} break;
			
				case 2:
				{
					assert( sCommands[index] == "run_server" );
					bMgr.RunServer();
				} break;
			}
		}
    }
    return 0;
}
